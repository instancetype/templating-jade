/**
 * Created by instancetype on 6/20/14.
 */
var jade = require('jade')
  , template = 'strong #{message}'
  , context = {message: 'Hello template!'}

var fn = jade.compile(template)
console.log(fn(context))