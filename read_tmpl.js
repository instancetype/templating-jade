/**
 * Created by instancetype on 6/20/14.
 */
var jade = require('jade')
  , fs = require('fs')
  , template = fs.readFileSync('./tmpl_to_read.jade')

  , context = { messages: [ 'You have logged in successfully'
                          , 'Welcome back!'
                          ]}

var fn = jade.compile(template)
console.log(fn(context))