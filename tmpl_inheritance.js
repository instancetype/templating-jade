/**
 * Created by instancetype on 6/20/14.
 */
var jade = require('jade')
  , fs = require('fs')
  , templateFile = './template/page.jade'
  , iterTemplate = fs.readFileSync(templateFile)

var context = { messages: [ 'You have logged in successfully!'
                          , 'Welcome back!'
                          ]}

var iterFn = jade.compile(
  iterTemplate,
  {filename: templateFile}
)

console.log(iterFn(context))